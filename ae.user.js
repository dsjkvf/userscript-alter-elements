// ==UserScript==
// @name         alter elements
// @description  alter elements
// @namespace    oyvey
// @author       dsjkvf@gmail.com
// @homepage     https://bitbucket.org/dsjkvf/userscript-alter-elements
// @downloadURL  https://bitbucket.org/dsjkvf/userscript-alter-elements/raw/master/ae.user.js
// @updateURL    https://bitbucket.org/dsjkvf/userscript-alter-elements/raw/master/ae.user.js
// @match        http://www.vsetv.com/schedule*
// @run-at       document-end
// @grant        none
// @version      0.7
// ==/UserScript==



function highlightWord(mode, pattern, code) {
    /**
     *  Alters the display of an element, which is detectable by a provided pattern.
     *  Takes 3 parameters:
     *      - mode
     *      - pattern
     *      - code
     *  Allows to change:
     *      - the representation of the whole element by applying the provided CSS code to it (mode 'alter')
     *      - the representation of the provided pattern by applying the provided CSS code to it only (mode 'alter-pattern-only')
     *      - the representation of the whole element by injecting the provided HTML code before [and after] it (mode 'inject')
     *      - the representation of the provided pattern by injecting the provided HTML code before and after it only (mode 'inject-pattern-only')
     *  With some help from:
     *      - https://stackoverflow.com/a/32537280/1068046
     *      - https://stackoverflow.com/q/6304453/1068046
     */
    var xpath = "//text()[contains(., '" + pattern + "')]";
    var texts = document.evaluate(xpath, document.body, null, XPathResult.UNORDERED_NODE_SNAPSHOT_TYPE, null);
    for (n = 0; n < texts.snapshotLength; n++) {
        var textNode = texts.snapshotItem(n);
        var textCont = textNode.textContent
        var p = textNode.parentNode;
        var x = document.createDocumentFragment();
        var nodeNew = document.createElement('span');
        switch (mode) {
            case 'alter':
                nodeNew.style.setProperty(code.split(':')[0], code.split(':')[1])
                nodeNew.appendChild(document.createTextNode(textCont));
                x.appendChild(nodeNew);
                break;
            case 'alter-pattern-only':
                nodeNew.style.setProperty(code.split(':')[0], code.split(':')[1])
                nodeNew.appendChild(document.createTextNode(textCont.substr(0, pattern.length)));
                x.appendChild(nodeNew);
                x.appendChild(document.createTextNode(textCont.substr(pattern.length)));
                break;
            case 'inject':
                if (code.split('|').length == 2) {
                    nodeNew.innerHTML = code.split('|')[0] + textCont + code.split('|')[1]
                } else {
                    nodeNew.innerHTML = code + textCont
                }
                x.appendChild(nodeNew);
                break;
            case 'inject-pattern-only':
                if (code.split('|').length == 2) {
                    nodeNew.innerHTML = code.split('|')[0] + textCont.substr(0, pattern.length) + code.split('|')[1] + textCont.substr(pattern.length)
                }
                x.appendChild(nodeNew);
                break;
        }
        p.replaceChild(x, textNode);
    }
}

if (window.location.hostname.includes('vsetv.com')) {
    highlightWord('inject', 'Фильм-спектакль', '<img src="pic/f.gif" width="13" height="11" align="absmiddle">&nbsp;<b>|</b>');
    highlightWord('inject', 'Телеспектакль', '<img src="pic/f.gif" width="13" height="11" align="absmiddle">&nbsp;<b>|</b>');
    highlightWord('alter-pattern-only', 'Тинькофф ', 'font-size:0');
};
